<?php
/**
 * @file
 * view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function view_views_api() {
  return array("version" => "3.0");
}
