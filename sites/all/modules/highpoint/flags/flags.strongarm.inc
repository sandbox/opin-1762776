<?php
/**
 * @file
 * flags.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function flags_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'flag_default_flag_status';
  $strongarm->value = array(
    'like' => TRUE,
  );
  $export['flag_default_flag_status'] = $strongarm;

  return $export;
}
