<?php
/**
 * @file
 * contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'all';
  $context->description = 'A context which is applied to all pages sitewide.';
  $context->tag = 'Highpoint';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'header',
          'weight' => '-10',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'menu-menu-favourites' => array(
          'module' => 'menu',
          'delta' => 'menu-favourites',
          'region' => 'sidebar_first',
          'weight' => '-12',
        ),
        'views-discussion_archive-block' => array(
          'module' => 'views',
          'delta' => 'discussion_archive-block',
          'region' => 'sidebar_first',
          'weight' => '-11',
        ),
        'views-count_discussion_content-block' => array(
          'module' => 'views',
          'delta' => 'count_discussion_content-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-e95b76238d382f0237cb093771f945b4' => array(
          'module' => 'views',
          'delta' => 'e95b76238d382f0237cb093771f945b4',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-26367b846de6af2abd97cbb2d76f4362' => array(
          'module' => 'views',
          'delta' => '26367b846de6af2abd97cbb2d76f4362',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A context which is applied to all pages sitewide.');
  t('Highpoint');
  $export['all'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'discussion_feed';
  $context->description = 'A context to manage the display of the discussion feed.';
  $context->tag = 'Highpoint';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
        'discussions' => 'discussions',
        'discussions/most-liked' => 'discussions/most-liked',
        'discussions/most-discussed' => 'discussions/most-discussed',
      ),
    ),
  );
  $context->reactions = array(
    'breadcrumb' => 'discussions',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A context to manage the display of the discussion feed.');
  t('Highpoint');
  $export['discussion_feed'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'discussion_item';
  $context->description = 'A context that handles the display of all discussion items.';
  $context->tag = 'Highpoint';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'discussion' => 'discussion',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'breadcrumb' => 'discussions',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A context that handles the display of all discussion items.');
  t('Highpoint');
  $export['discussion_item'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home';
  $context->description = 'A context to manage the items that appear specifically on the homepage.';
  $context->tag = 'Highpoint';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A context to manage the items that appear specifically on the homepage.');
  t('Highpoint');
  $export['home'] = $context;

  return $export;
}
