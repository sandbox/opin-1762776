<?php
/**
 * @file
 * content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_types_node_info() {
  $items = array(
    'discussion' => array(
      'name' => t('Discussion'),
      'base' => 'node_content',
      'description' => t('A discussion can be created to get the feedback from system users by vote or commenting.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webpage' => array(
      'name' => t('Webpage'),
      'base' => 'node_content',
      'description' => t('A webpage for providing information throughout the system.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
